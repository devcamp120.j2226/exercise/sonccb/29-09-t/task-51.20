package com.devcamp.j20_javabasic.s20.ArrayList_and_for;

import java.util.ArrayList;

public class forExample {
 public static void main(String[] args) {
  ArrayList<Integer> arrayList = new ArrayList<Integer>();
  arrayList.add(25);
  arrayList.add(13);
  arrayList.add(35);
  arrayList.add(54);
  arrayList.add(56);
  arrayList.add(45);
  arrayList.add(58);
  arrayList.add(87);
  arrayList.add(65);
  arrayList.add(88);
  arrayList.add(37);
  arrayList.add(99);
  for (int i = 0; i < arrayList.size(); i++) {
   System.out.println(arrayList.get(i));
  }
 }
}
