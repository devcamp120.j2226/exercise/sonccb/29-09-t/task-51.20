package com.devcamp.j20_javabasic.s20.array_and_for;

// import java.util.ArrayList;
import java.util.Random;

public class forExample {
 public static void newArrayList() {
  Random random = new Random();
  Integer[] arrayList = new Integer[20];
  //Create list number
  for (int i = 0; i < 20; i++) {
   arrayList[i] = Integer.valueOf(random.nextInt());
  }
  for (int i = 0; i < arrayList.length; i++) {
   System.out.println(arrayList[i]);
  }
 }
 public static void main(String[] args) {
  forExample.newArrayList();
 }
}
