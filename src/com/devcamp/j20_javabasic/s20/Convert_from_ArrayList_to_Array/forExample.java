package com.devcamp.j20_javabasic.s20.Convert_from_ArrayList_to_Array;

import java.util.ArrayList;
// import java.util.Arrays;
// import java.util.Collections;
// import java.util.Random;

public class forExample {
 public static void ToArray() {
  ArrayList<Integer> arrayList = new ArrayList<>();
  arrayList.add(1);
  arrayList.add(2);
  arrayList.add(3);
  Integer[] results = arrayList.stream().toArray(size -> new Integer[size]);
  for (Integer i : results) {
   System.out.println(i + " ");
  }
 }
 public static void main(String[] args) {
  forExample.ToArray();
 }
}
