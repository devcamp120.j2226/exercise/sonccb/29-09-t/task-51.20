package com.devcamp.j20_javabasic.s20.Convert_array_to_arraylist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
// import java.util.Random;

public class forExample {
 public static void arrayToArrayList() {
  String[] strings = new String[] {"a", "b" , "c" , "d"};
  ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(strings));
  System.out.println(arrayList);
 }
 public static void arrayToArrayList1() {
  Integer[] arr = { 1, 2, 3, 4, 5, 6, 7 };
  ArrayList<Integer> arrayList = new ArrayList<>();
  Collections.addAll(arrayList, arr);
  System.out.println(arrayList);
 }
 public static void arrayToArrayList2() {
  Integer[] arr = { 1, 2, 3, 4, 5, 6, 7 };
  ArrayList<Integer> arrayList = new ArrayList<>();
  for (Integer i : arr) {
   arrayList.add(i);
  }
  System.out.println(arrayList);
 }
 public static void main(String[] args) {
  forExample.arrayToArrayList();
  forExample.arrayToArrayList1();
  forExample.arrayToArrayList2();
 }
}
